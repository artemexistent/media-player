/*
 *  Copyright 2022, TeamDev. All rights reserved.
 *
 *  Redistribution and use in source and/or binary forms, with or without
 *  modification, must retain the above copyright notice and the following
 *  disclaimer.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package ua.knu.mediaplayer;

import com.teamdev.jxbrowser.frame.Frame;
import com.teamdev.jxbrowser.js.JsAccessible;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Consumer;

final class JsPlayer {

    private final Frame frame;

    private final Set<Consumer<Double>> timeUpdatedListeners;
    private final Set<Consumer<Boolean>> volumeChangedListeners;
    private final Set<Consumer<Void>> playbackStartedListeners;
    private final Set<Consumer<Void>> playbackPausedListeners;

    JsPlayer(Frame frame) {
        this.frame = frame;

        timeUpdatedListeners = new CopyOnWriteArraySet<>();
        playbackStartedListeners = new CopyOnWriteArraySet<>();
        playbackPausedListeners = new CopyOnWriteArraySet<>();
        volumeChangedListeners = new CopyOnWriteArraySet<>();
    }

    private <T> T execute(String s) {
        return frame.executeJavaScript(s);
    }


    void play() {
        execute("player.play()");
    }

    void pause() {
        execute("player.pause()");
    }

    Boolean isPlaying() {
        return execute("player.playing");
    }

    Double duration() {
        return execute("player.duration");
    }

    void currentTime(double timeInSeconds) {
        execute("player.currentTime = " + timeInSeconds);
    }

    Double currentTime() {
        return execute("player.currentTime");
    }

    void volume(double volume) {
        execute("player.volume = " + volume);
    }

    Double volume() {
        return execute("player.volume");
    }

    void mute() {
        execute("player.muted = true");
    }

    void unmute() {
        execute("player.muted = false");
    }

    Boolean isMuted() {
        return execute("player.muted");
    }

    void onPlaybackStarted(Consumer<Void> listener) {
        playbackStartedListeners.add(listener);
    }

    void onPlaybackPaused(Consumer<Void> listener) {
        playbackPausedListeners.add(listener);
    }

    void onTimeUpdated(Consumer<Double> listener) {
        timeUpdatedListeners.add(listener);
    }

    void onVolumeChanged(Consumer<Boolean> listener) {
        volumeChangedListeners.add(listener);
    }

    @JsAccessible
    public void onPlaybackStarted() {
        playbackStartedListeners.forEach(listener -> listener.accept(null));
    }

    @JsAccessible
    public void onPlaybackPaused() {
        playbackPausedListeners.forEach(listener -> listener.accept(null));
    }

    @JsAccessible
    public void onTimeUpdated(double currentTime) {
        timeUpdatedListeners.forEach(listener -> listener.accept(currentTime));
    }

    @JsAccessible
    public void onVolumeChanged(boolean muted) {
        volumeChangedListeners.forEach(listener -> listener.accept(muted));
    }
}
