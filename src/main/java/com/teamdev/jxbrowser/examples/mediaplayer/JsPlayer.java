package com.teamdev.jxbrowser.examples.mediaplayer;

import com.teamdev.jxbrowser.frame.Frame;
import com.teamdev.jxbrowser.js.JsAccessible;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Consumer;

final class JsPlayer {

    private final Frame frame;

    private final Set<Consumer<Double>> timeUpdatedListeners;
    private final Set<Consumer<Boolean>> volumeChangedListeners;
    private final Set<Consumer<Void>> playbackStartedListeners;
    private final Set<Consumer<Void>> playbackPausedListeners;

    JsPlayer(Frame frame) {
        this.frame = frame;

        timeUpdatedListeners = new CopyOnWriteArraySet<>();
        playbackStartedListeners = new CopyOnWriteArraySet<>();
        playbackPausedListeners = new CopyOnWriteArraySet<>();
        volumeChangedListeners = new CopyOnWriteArraySet<>();
    }

    private <T> T execute(String s) {
        return frame.executeJavaScript(s);
    }


    void play() {
        execute("player.play()");
    }

    void pause() {
        execute("player.pause()");
    }

    Boolean isPlaying() {
        return execute("player.playing");
    }

    Double duration() {
        return execute("player.duration");
    }

    void currentTime(double timeInSeconds) {
        execute("player.currentTime = " + timeInSeconds);
    }

    Double currentTime() {
        return execute("player.currentTime");
    }

    void volume(double volume) {
        execute("player.volume = " + volume);
    }

    Double volume() {
        return execute("player.volume");
    }

    void mute() {
        execute("player.muted = true");
    }

    void unmute() {
        execute("player.muted = false");
    }

    Boolean isMuted() {
        return execute("player.muted");
    }

    void onPlaybackStarted(Consumer<Void> listener) {
        playbackStartedListeners.add(listener);
    }

    void onPlaybackPaused(Consumer<Void> listener) {
        playbackPausedListeners.add(listener);
    }

    void onTimeUpdated(Consumer<Double> listener) {
        timeUpdatedListeners.add(listener);
    }

    void onVolumeChanged(Consumer<Boolean> listener) {
        volumeChangedListeners.add(listener);
    }

    @JsAccessible
    public void onPlaybackStarted() {
        playbackStartedListeners.forEach(listener -> listener.accept(null));
    }

    @JsAccessible
    public void onPlaybackPaused() {
        playbackPausedListeners.forEach(listener -> listener.accept(null));
    }

    @JsAccessible
    public void onTimeUpdated(double currentTime) {
        timeUpdatedListeners.forEach(listener -> listener.accept(currentTime));
    }

    @JsAccessible
    public void onVolumeChanged(boolean muted) {
        volumeChangedListeners.forEach(listener -> listener.accept(muted));
    }
}
